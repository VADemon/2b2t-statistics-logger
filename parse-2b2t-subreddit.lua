#!/usr/bin/env lua

-- The div.md is reddits subreddit info bar for old design.
--[[<div class="md"><p>2builders2tools is a minecraft server with the goal to never reset the world in a free for all no rules pvp environment, with little modification to the vanilla survival gamemode. The world is nine years and three months old, with a size of 7468 GBs and over 456,213 players visiting at least once. The IP to connect is <strong>2b2t.org</strong></p>

<p>&nbsp;</p>
.....
</div>
]]

local dbg = false or (os.getenv("debug") and true)

function writeToFile(filename, data)
	if dbg then print(string.format("Writing to file '%s'", filename)) end
	local fileData = assert(io.open(filename, "w"))
	fileData:write(data)
	fileData:close()
end

function fileContentEquals(filename, data)
	local fileData, err = io.open(filename, "rb")
	if err then return false end -- early return if file missing
	
	local all = fileData:read("*a")
	fileData:close()
	if all == data then
		if dbg then print(string.format("File content is equal for file '%s'", filename)) end
		return true
	end
	return false
end
function compareAndUpdateFile(dataNameFancy, dataName, data, date)
	if not fileContentEquals("last_".. dataName ..".txt", data) then
		writeToFile("data/".. date .."_".. dataName ..".txt", data)
		writeToFile("last_".. dataName ..".txt", data)
		print(dataNameFancy .." was updated: ".. data)
	end
end

local url = "https://old.reddit.com/r/2b2t/"
curlArgs = ' -L -A "Mozilla/5.0 (X11; Linux x86_64; rv:87.0) Gecko/20100101 Firefox/87.0" -H "Connection: close" '
local p = io.popen("curl --silent ".. curlArgs .." ".. url, "r")

local html = p:read("*a")

if #html < 4 then
	local perr = io.popen("curl --verbose ".. curlArgs .." "..  url, "r")
	io.stderr:write("Couldn't fetch page: ".. url .."\nCurl verbose output below:\n")
	io.stderr:write(perr:read("*a"))
	os.exit(1)
end

divmd = html:match([[<div class="md">(.-)</div>]])
if not divmd then
	io.stderr:write("Couldn't grab div.md!\n")
	io.stderr:write("Page says (title): ".. (html:match("<title>(.-)</title>") or "#Couldnt grab title!#"))
	os.exit(1)
end

divmd = divmd:gsub("\r\n", "\n") -- normalize
divmdLower = divmd:lower()

os.execute("[ -d data ] || mkdir data")

local date =  os.date("%Y-%m-%d_%H-%M")
local worldSize = divmdLower:match("size of ([^ ]+ [^ ]+)")
local playerCount = divmdLower:match("([%d%.,]+) players?")
local worldDataString = worldSize .."|".. playerCount

compareAndUpdateFile("Worlddata", "worlddata", worldDataString, date)
compareAndUpdateFile("Divmd", "divmd_dump", divmd, date)
