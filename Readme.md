# Script to record stats from r/2b2t sidebar

### Installation:
Unix-shell only.

You only need some Lua and curl. Here's an example for Debian/Ubuntu:

`sudo apt install -y curl lua`

### Start
Launch with ./parse-2b2t-subreddit.lua

It will automatically only record CHANGES (diffs) to previous state. Historical data will be saved to folder `data/`

### Crontab
`crontab -e`: `30 *		* * * (cd $YOUR_FULL_PATH_HERE/2b2t-statistics-logger/ && ./parse-2b2t-subreddit.lua)`

The script will ONLY output to stdout/stderr: in case of data updates and in case of errors to retrieve data (if you have setup e-mail on the host, you will receive notifications).